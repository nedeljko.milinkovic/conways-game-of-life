#include "stdafx.h"
#include <iostream>
#include <windows.h>
#define MIN_GAME_MATRIX_WIDTH 5
#define MIN_GAME_MATRIX_HEIGHT 5
#define MAX_GAME_MATRIX_WIDTH 50
#define MAX_GAME_MATRIX_HEIGHT 50
using namespace std;
void gotoXY(int x, int y)
{
	//Initialize the coordinates
	COORD coord = { x, y };
	//Set the position
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	return;
}

//Get the handle to the current output buffer...
HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

void ClearConsole()
{
	//This is used to reset the carat/cursor to the top left.
	COORD coord = { 0, 0 };
	//A return value... indicating how many chars were written
	//   not used but we need to capture this since it will be
	//   written anyway (passing NULL causes an access violation).
	DWORD count;
	//This is a structure containing all of the console info
	// it is used here to find the size of the console.
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	//Here we will set the current color
	if (GetConsoleScreenBufferInfo(hStdOut, &csbi))
	{
		//This fills the buffer with a given character (in this case 32=space).
		FillConsoleOutputCharacter(hStdOut, (TCHAR)32, csbi.dwSize.X * csbi.dwSize.Y, coord, &count);
		FillConsoleOutputAttribute(hStdOut, csbi.wAttributes, csbi.dwSize.X * csbi.dwSize.Y, coord, &count);
		//This will set our cursor position for the next print statement.
		SetConsoleCursorPosition(hStdOut, coord);
	}
	return;
}


struct Point {
	int x;
	int y;
};

struct Region {
	int xUpperLeft;
	int yUpperLeft;
	int xLowerRight;
	int yLowerRight;
};

bool pointInRegion(Point testnaTacka, Region r) {
	if (testnaTacka.x < r.xUpperLeft || testnaTacka.x > r.xLowerRight)
		return false;

	if (testnaTacka.y < r.yUpperLeft || testnaTacka.y > r.yLowerRight)
		return false;

	return true;
}

void DrawBoard(bool **board, int width, int height) {
	gotoXY(0, 0);
	for (int i = 0; i < width; ++i)
	{
		for (int j = 0; j < height; ++j) {
			if (board[i][j]) {
				SetConsoleTextAttribute(hStdOut, 10);
				cout << "X";
			}
			else {
				SetConsoleTextAttribute(hStdOut, 15);
				cout << "0";
			}
		}
		cout << endl;
	}
	cout.flush();
}

int getLiveNeighborCount(int cellX, int cellY, bool **board, int width, int height) {
	Region r = { 0,0,width - 1,height - 1 };
	int live = 0;

	Point p = { cellX - 1, cellY };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX - 1, cellY - 1 };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX, cellY - 1 };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX + 1, cellY - 1 };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX + 1, cellY };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX + 1, cellY + 1 };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX, cellY + 1 };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	p = { cellX - 1, cellY + 1 };
	if (pointInRegion(p, r) && board[p.x][p.y])
		live++;

	return live;
}

void Play(bool **board, int width, int height) {
	bool **tempBoard = new bool*[width];
	for (int i = 0; i < width; ++i) {
		tempBoard[i] = new bool[height];
		for (int j = 0; j < height; ++j) {
			tempBoard[i][j] = board[i][j];
		}
	}


	for (int i = 0; i < width; ++i) {
		for (int j = 0; j < height; ++j)
		{
			int liveNeighbors = getLiveNeighborCount(i, j, tempBoard, width, height);
			if (tempBoard[i][j] == false)
			{
				if (liveNeighbors == 3)
				{
					//Revive a dead cell with exactly 3 live neighbors
					board[i][j] = true;
					continue;
				}
			}
			else
			{
				switch (liveNeighbors)
				{
				case 0:
				case 1:
					board[i][j] = false;
					break;
				case 2:
				case 3:
					// Do nothing, cell remains alive
					break;
				default:
					board[i][j] = false;
					break;
				}
			}
		}
	}




}
void EraseBoard(bool **board, int width, int height) {
	for (int i = 0; i < width; ++i)
	{
		for (int j = 0; j < height; ++j) {
			board[i][j] = false;
		}
	}
}


int main() {

	// Go to https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life to find life forms :)
	ClearConsole();

	bool ** gameMatrix;
	int gameMatrixWidth, gameMatrixHeight;


	cout << "Unesi dimenzije ploce: " << endl;
	cin >> gameMatrixWidth >> gameMatrixHeight;
	if (gameMatrixWidth < MIN_GAME_MATRIX_WIDTH || gameMatrixHeight < MIN_GAME_MATRIX_HEIGHT) {
		cout << "Najmanja dozvoljena dimenzija ploce: " << MIN_GAME_MATRIX_WIDTH << "x" << MIN_GAME_MATRIX_HEIGHT << endl;
		return -1;
	}
	if (gameMatrixWidth > MAX_GAME_MATRIX_WIDTH || gameMatrixHeight > MAX_GAME_MATRIX_HEIGHT) {
		cout << "Najveca dozvoljena dimenzija ploce: " << MAX_GAME_MATRIX_WIDTH << "x" << MAX_GAME_MATRIX_HEIGHT << endl;
		return -1;
	}

	gameMatrix = new bool*[gameMatrixWidth];
	for (int i = 0; i < gameMatrixWidth; ++i) {
		gameMatrix[i] = new bool[gameMatrixHeight];
	}

	Region tabla = { 0,0, gameMatrixWidth, gameMatrixHeight };
	EraseBoard(gameMatrix, gameMatrixWidth, gameMatrixHeight);

	cout << "Unesite pozicije zivih celija prije pocetka igre ( za prekid unosa, unesite -1 kao bilo koju koordinatu): " << endl;;
	while (true) {
		Point temp;
		cin >> temp.x >> temp.y;

		if (temp.x == -1 || temp.y == -1)
			break;

		temp.x--;
		temp.y--;

		if (!pointInRegion(temp, tabla)) {
			cout << "Pozicija nije ispravna. Celija nije dodana." << endl;
			continue;
		}
		gameMatrix[temp.x][temp.y] = true;
	}

	cout.flush();
	ClearConsole();
	
	//Increase iteration count
	const int ITERATION_COUNT = 1000;

	for (int i = 0; i < ITERATION_COUNT; i++) {
		DrawBoard(gameMatrix, gameMatrixWidth, gameMatrixHeight);
		Play(gameMatrix, gameMatrixWidth, gameMatrixHeight);
		Sleep(500); 
	}
	return 0;
}
